
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kitti
 */
public class Main {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] XO = new char[3][3];

        boolean X = false;
        boolean O = false;
        boolean result = false;

        int round = 1;

        while (result == false) {
//            System.out.println(round);
            if (round % 2 == 1) {
                System.out.println("Turn X : ");
            } else if (round % 2 == 0) {
                System.out.println("Turn O : ");
            }
            int r = kb.nextInt();
            int c = kb.nextInt();
            for (int row = 1; row < XO.length + 1; row++) {
                for (int col = 1; col < XO.length + 1; col++) {
                    if (row == r && col == c) {
                        if (round % 2 == 1) {
                            XO[row - 1][col - 1] = 'X';
                        } else if (round % 2 == 0) {
                            XO[row - 1][col - 1] = 'O';
                        }

                    }
                }
            }
            round++;
            for (int row = 0; row < XO.length; row++) {
                for (int col = 0; col < XO.length; col++) {
                    if (XO[row][col] == 'X') {
                        System.out.print("X ");
                    } else if (XO[row][col] == 'O') {
                        System.out.print("O ");
                    } else {
                        System.out.print("- ");
                    }
                }
                System.out.println();
            }
            if (XO[0][0] == 'X' && XO[1][0] == 'X' && XO[2][0] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][0] == 'O' && XO[1][0] == 'O' && XO[2][0] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][1] == 'X' && XO[1][1] == 'X' && XO[2][1] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][1] == 'O' && XO[1][1] == 'O' && XO[2][1] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][2] == 'X' && XO[1][2] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][2] == 'O' && XO[1][2] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][0] == 'X' && XO[0][1] == 'X' && XO[0][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][0] == 'O' && XO[0][1] == 'O' && XO[0][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[1][0] == 'X' && XO[1][1] == 'X' && XO[1][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[1][0] == 'O' && XO[1][1] == 'O' && XO[1][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[2][0] == 'X' && XO[2][1] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[2][0] == 'O' && XO[2][1] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][2] == 'X' && XO[1][1] == 'X' && XO[2][0] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][2] == 'O' && XO[1][1] == 'O' && XO[2][0] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            } else if (XO[0][0] == 'X' && XO[1][1] == 'X' && XO[2][2] == 'X') {
                System.out.println(">>> X win <<<");
                break;
            } else if (XO[0][0] == 'O' && XO[1][1] == 'O' && XO[2][2] == 'O') {
                System.out.println(">>> O win <<<");
                break;
            }else{
                System.out.println(">>> Draw <<<");
            }
        }
    }
}
